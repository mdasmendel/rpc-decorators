# RPC decorators &middot;

A Nest.js library that stores oftenly used decorators.

## Built With

- @nestjs/microservices
- @nestjs/common

## Installing / Getting started

A quick introduction of the minimal setup you need to do for running.

1. Clone library

```shell
git clone git@gitlab.com:system_0/rpc-decorators.git
```

2. Install required packages
```
cd project

yarn
```


## Developing

In order to start project you should run following command:

```shell
yarn start
```

## Building

To respect style guide and avoid erorrs on production you have to check build status. If script has run with no errors you are free to go further.

Check build status script:

```
yarn check:build
```

Build script:

```shell
yarn build
```

After yarn build script ran, in project root will appear build folder.


## Versioning

rpc-decorators@1.0

## Configuration

In order to run project you must have installed:

- git
- yarn
- IDE



## Style guide

Code style is validated by eslint with recommended plugin for Typescript.

How to check code style:

```shell
yarn lint
```
