import { InternalServerErrorException } from '@nestjs/common';
import { RpcException } from '@nestjs/microservices';

const handleRPC = () => {
  return (
    target: unknown,
    propertyKey: string,
    descriptor: TypedPropertyDescriptor<
      (...params: unknown[]) => Promise<unknown>
    >,
  ): void => {
    if (!descriptor.value) {
      throw new RpcException('Descriptor value not found');
    }

    const oldFunc = descriptor.value;
    descriptor.value = async function (...args) {
      try {
        return await oldFunc.apply(this, args);
      } catch (e) {
        if (!e.status) {
          throw new InternalServerErrorException(e.message || e);
        }
        return new RpcException(e.response || e);
      }
    };
  };
};

export { handleRPC };
